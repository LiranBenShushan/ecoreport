package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.w3c.dom.Text;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class addReportDetails extends AppCompatActivity {
    public static final String TEXT = "com.example.myapplication.example.TEXT";
    public static final String REPORT = "com.example.myapplication.example.REPORT";
    public static final String SNIPPET = "com.example.myapplication.example.SNIPPET";
    public static final String CATEGORY = "com.example.myapplication.example.CATEGORY";
    private String latitude;
    private String longtitude;
    static String textImage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_report_details);

        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }

        Intent intent = getIntent();
        String text = intent.getStringExtra(ReportFromMap.TEXT);
        textImage = intent.getStringExtra(ReportFromMap.TEXTIMAGE);
        latitude = intent.getStringExtra(ReportFromMap.myCurrentLocation_latitude);
        longtitude = intent.getStringExtra(ReportFromMap.myCurrentLocation_longtitude);

        TextView reportName = (TextView) findViewById(R.id.reportName2);
        reportName.setText(text);

        ImageView pullotantPicture = (ImageView) findViewById(R.id.imageView5);
        int pullotantResource = getResources().getIdentifier("@drawable/"+textImage,null, this.getPackageName());
        pullotantPicture.setImageResource(pullotantResource);

        try {
            weatherForcast();
        } catch (IOException e) {
            e.printStackTrace();
        }

        String currentDate = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(new Date());
        String currentTime = new SimpleDateFormat("HH:mm", Locale.getDefault()).format(new Date());
        TextView time_label = (TextView) findViewById(R.id.time_label);
        time_label.setText("תאריך: " + currentDate + " | "+ currentTime);
    }
    public void weatherWebSite(View v) throws IOException {
        String url = "https://weather.com/weather/today/l/32.52,34.95?par=google&temp=c";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }


    public void weatherForcast() throws IOException {
        new Thread(new Runnable(){
            @Override
            public void run() {
                Document doc = null;
                String temperture = null;
                String weather = null;
                String time = null;
                try {
                    doc = Jsoup.connect("https://weather.com/weather/today/l/32.52,34.95?par=google&temp=c").get();
                    temperture = doc.select("span[class=CurrentConditions--tempValue--3KcTQ]").text().toString();
                    weather = doc.select("div[class=CurrentConditions--phraseValue--2xXSr]").text().toString();
                    time =  doc.select("div[class=CurrentConditions--timestamp--1SWy5]").text().toString();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                updateWeather(temperture, weather, time);
            }}).start();
    }

    public void updateWeather(String temperture, String weather, String time){
        TextView weatherForcast = findViewById(R.id.weatherForcast2);
        weatherForcast.setText(weather + " " + temperture + " מעלות");
        ImageView weatherPicture = (ImageView) findViewById(R.id.imageView34);
        if(weather.equals("Sunny"))
            if(time.contains("am"))
                weatherPicture.setImageResource(getResources().getIdentifier("@drawable/status_weather_clear_icon",null, this.getPackageName()));
            else
                weatherPicture.setImageResource(getResources().getIdentifier("@drawable/status_weather_clear_night_icon",null, this.getPackageName()));
        else if(weather.equals("Sunny"))
            weatherPicture.setImageResource(getResources().getIdentifier("@drawable/status_weather_clear_icon",null, this.getPackageName()));
    }


    public void returnToMap(View v){

        TextView report = (TextView) findViewById(R.id.reportName2);
        TextView snippet = (TextView) findViewById(R.id.editTextTextMultiLine);
        Intent i = new Intent(this, MapsActivity.class);
        i.putExtra(TEXT,"StartLocation");
        i.putExtra(REPORT,report.getText().toString());
        if(snippet.getText().toString().equals("כתוב דיווח") || snippet.getText().toString().equals(" "))
            i.putExtra(SNIPPET," ");
        else
            i.putExtra(SNIPPET,snippet.getText().toString());
        i.putExtra(CATEGORY,textImage);
        startActivity(i);
    }
}