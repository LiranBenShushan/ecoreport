package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class SavedLocations extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saved_locations);


        /*Picture to background*/
        ImageView backgroundPicture = (ImageView) findViewById(R.id.backgroundImage);
        int imagebackgroundResourse = getResources().getIdentifier("@drawable/historypagebackground",null, this.getPackageName());
        backgroundPicture.setImageResource(imagebackgroundResourse);
    }

    public void homePage(View v){
        Intent i = new Intent(this, MapsActivity.class);
        startActivity(i);
    }

}