package com.example.myapplication;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

public class AddReport extends AppCompatActivity {
    public static final String TEXT = "com.example.myapplication.example.TEXT";
    public static final String TEXTIMAGE = "com.example.myapplication.example.TEXTIMAGE";
    public static final String myCurrentLocation_latitude = "com.example.myapplication.example.myCurrentLocation_latitude";
    public static final String myCurrentLocation_longtitude = "com.example.myapplication.example.myCurrentLocation_longtitude";
    private String latitude;
    private String longtitude;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_add_report);
        //Toolbar toolbar = findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);

//        FloatingActionButton fab = findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });



        Intent intent = getIntent();
        String text = intent.getStringExtra(ReportFromMap.TEXT);
        latitude = intent.getStringExtra(ReportFromMap.myCurrentLocation_latitude);
        longtitude = intent.getStringExtra(ReportFromMap.myCurrentLocation_longtitude);
        TextView tv = (TextView) findViewById(R.id.reportName);
        tv.setText(text);


        /*Picture to pullotant*/
        ImageView pullotantPicture = (ImageView) findViewById(R.id.imageView);
        String textImage = intent.getStringExtra(ReportFromMap.TEXTIMAGE);
        int pullotantResourse = getResources().getIdentifier("@drawable/"+textImage,null, this.getPackageName());
        pullotantPicture.setImageResource(pullotantResourse);


        Button firstPullotant = (Button) findViewById(R.id.buttonFirstKind);
        Button secondPullotant = (Button) findViewById(R.id.buttonSecondKind);
        Button thirdPullotant = (Button) findViewById(R.id.buttonThirdKind);
        Button fourthPullotant = (Button) findViewById(R.id.buttonFourthKind);

        TextView explainPolutant1 = (TextView) findViewById(R.id.explainPolutant1);
        TextView explainPolutant2 = (TextView) findViewById(R.id.explainPolutant2);
        TextView explainPolutant3 = (TextView) findViewById(R.id.explainPolutant3);
        TextView explainPolutant4 = (TextView) findViewById(R.id.explainPolutant4);

        switch (intent.getStringExtra(ReportFromMap.TEXT)){
            case "ריח":
                firstPullotant.setText("אוזון");
                explainPolutant1.setText("מאופיין בריח תעשייתי כימי חריף. קיימת סבירות גבוהה לריח זה בקרבת כבישים גדושים ובאזורים נשר, חיפה והקריות.");
                secondPullotant.setText("דלקים");
                explainPolutant2.setText("בנזן מצוי באדי הדלקים ובעל ריח אופייני לדלקים. קיימת סבירות גבוהה לריח זה באזור הקריות.");
                thirdPullotant.setText("בהמות");
                explainPolutant3.setText("ריח צואה של חיות. קיימת סבירות גבוהה לריח זה בקרבת חוות, אורוות וכדומה.");
                fourthPullotant.setText("אחר");
                explainPolutant4.setText("השתמש באפשרות זו במידה ואינך מזהה את הריח");
                break;
            case "עשן/זיהום אוויר":
                firstPullotant.setText("שריפה");
                explainPolutant1.setText("");
                secondPullotant.setText("עשן");
                explainPolutant2.setText("");
                thirdPullotant.setText("ערפל");
                explainPolutant3.setText("");
                fourthPullotant.setText("אחר");
                explainPolutant4.setText("השתמש באפשרות זו במידה ואינך מזהה את הריח");
                break;
            case "פסולת":
                firstPullotant.setText("פסולת בניין");
                explainPolutant1.setText("תוצר לוואי של עבודת בניה, סלילה ושיפוץ ומהווה חלק גדול מהיקף האשפה הכללי.");
                secondPullotant.setText("זבל עירוני");
                explainPolutant2.setText("פסולת עירונית של התושבים");
                thirdPullotant.setText("צואה");
                explainPolutant3.setText("צואה של בעלי חיים בסביבה עירונית.");
                fourthPullotant.setText("אחר");
                explainPolutant4.setText("השתמש באפשרות זו במידה ואינך מזהה את הריח");
                break;
            case "רעש":
                firstPullotant.setText("מסיבה רועשת");
                explainPolutant1.setText("מסיבה רועשת שאינה מאושרת בקרב בתי מגורים.");
                secondPullotant.setText("כלי רכב");
                explainPolutant2.setText("רעש הנובע מכלי רכב רועש/ים שלא בכבישים מרכזיים");
                thirdPullotant.setText("בנייה");
                explainPolutant3.setText("רעש הנובע מעבודות בנייה בסמוך לבתי מגורים.");
                fourthPullotant.setText("תעשייה");
                explainPolutant4.setText("רעש ממפעלים שנשמע מאזור בתי מגורים.");
                break;

        }
    }



    public void addReport(View v){
        String name = ((TextView)  findViewById(R.id.reportName)).getText().toString();
        TextView polutantText = null;
        switch (v.getId()){
            case R.id.buttonFirstKind:
                polutantText = (TextView) findViewById(R.id.buttonFirstKind);
                break;
            case R.id.buttonSecondKind:
                polutantText = (TextView) findViewById(R.id.buttonSecondKind);
                break;
            case R.id.buttonThirdKind:
                polutantText = (TextView) findViewById(R.id.buttonThirdKind);
                break;
            case R.id.buttonFourthKind:
                polutantText = (TextView) findViewById(R.id.buttonFourthKind);
                break;
        }
        checkClickedPolutant(name, polutantText);
    }

    public void checkClickedPolutant(String name, TextView polutantText) {
        if (name.equals("עשן/זיהום אוויר")) {
            openAddReporClass(polutantText, "company");
        }
        if (name.equals("ריח")) {
            openAddReporClass(polutantText, "smell");
        }
        if (name.equals("פסולת")) {
            openAddReporClass(polutantText, "company");
        }
        if (name.equals("רעש")) {
            openAddReporClass(polutantText, "ear");
        }
        if (name.equals("חומרים מסוכנים")) {
            openAddReporClass(polutantText, "biohazard");
        }
        if (name.equals("קרינה")) {
            openAddReporClass(polutantText, "nuclear");
        }
    }

    public void openAddReporClass(TextView textView, String name){
        String text = textView.getText().toString();
        Intent i = new Intent(this, addReportDetails.class);
        i.putExtra(TEXT,text);
        i.putExtra(TEXTIMAGE,name);
        i.putExtra(myCurrentLocation_latitude,latitude);
        i.putExtra(myCurrentLocation_longtitude,longtitude);
        startActivity(i);
    }

//    public void sentToWriteToCSV(View v) throws IOException {
//        InputStream is = getResources().openRawResource(R.raw.stations_locations);
////        MapsActivity.writeToCSV(is);
//        //        String myRow = getLocation().getLatitude() + "," + getLocation().getLongitude() + ","+ "דיווח 1" + "," + ",ריח לא טוב";
//        String myRow = "11" + "," + "222" + ","+ "דיווח 1" + "," + ",ריח לא טוב";
//        String [] location = myRow.split(",");
//
//            try {
//                OutputStreamWriter outputStreamWriter = new OutputStreamWriter(this.openFileOutput("stations_locations.csv", Context.MODE_PRIVATE));
//                outputStreamWriter.write(myRow);
//                outputStreamWriter.close();
//            }
//            catch (IOException e) {
//                Log.e("Exception", "File write failed: " + e.toString());
//            }
//
//    }

}