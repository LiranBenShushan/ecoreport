package com.example.myapplication;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class UserSection extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_section);

        Intent intent = getIntent();
        String usernameFromAccount = intent.getStringExtra(MapsActivity.User_NAME);

        TextView fullName = findViewById(R.id.full_name);
        fullName.setText(usernameFromAccount);

        TextView username = findViewById(R.id.user_name);
        username.setText("שם משתמש: " + usernameFromAccount);


        ImageView bubble_icon  = findViewById(R.id.bubble_icon);
        bubble_icon.setImageResource(getResources().getIdentifier("@drawable/sms_icon",null, this.getPackageName()));

        ImageView environment_flower_rank_icon  = findViewById(R.id.environment_flower_rank);
        environment_flower_rank_icon.setImageResource(getResources().getIdentifier("@drawable/environment_flower",null, this.getPackageName()));

        ImageView environment_trustee_rank_icon  = findViewById(R.id.environment_trustee_rank);
        environment_trustee_rank_icon.setImageResource(getResources().getIdentifier("@drawable/environment_trustee_big",null, this.getPackageName()));

        ImageView environment_guard_senior_rank_icon  = findViewById(R.id.environment_guard_senior_rank);
        environment_guard_senior_rank_icon.setImageResource(getResources().getIdentifier("@drawable/environment_guard_senior_black",null, this.getPackageName()));
    }
}