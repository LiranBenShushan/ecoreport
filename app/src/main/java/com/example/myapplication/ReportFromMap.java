package com.example.myapplication;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class ReportFromMap extends AppCompatActivity {
    public static final String TEXT = "com.example.myapplication.example.TEXT";
    public static final String TEXTIMAGE = "com.example.myapplication.example.TEXTIMAGE";
    public static final String myCurrentLocation_latitude = "com.example.myapplication.example.myCurrentLocation_latitude";
    public static final String myCurrentLocation_longtitude = "com.example.myapplication.example.myCurrentLocation_longtitude";
    private String latitude;
    private String longtitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_from_map);

        Intent intent = getIntent();
        latitude = intent.getStringExtra(MapsActivity.myCurrentLocation_latitude);
        longtitude = intent.getStringExtra(MapsActivity.myCurrentLocation_longtitude);
        createIconsForMainScreen();

    }



    public void createIconsForMainScreen(){

        /*Picture to smell*/
        ImageView smellPicture = (ImageView) findViewById(R.id.smell);
        int smellResourse = getResources().getIdentifier("@drawable/smell",null, this.getPackageName());
        smellPicture.setImageResource(smellResourse);


        /*Picture to Smoke*/
        ImageView SmokePicture = (ImageView) findViewById(R.id.Smoke);
        int SmokeimageResourse = getResources().getIdentifier("@drawable/company",null, this.getPackageName());
        SmokePicture.setImageResource(SmokeimageResourse);

        /*Picture to trash*/
        ImageView trashPicture = (ImageView) findViewById(R.id.trash);
        int trashimageResourse = getResources().getIdentifier("@drawable/trash",null, this.getPackageName());
        trashPicture.setImageResource(trashimageResourse);

        /*Picture to radiation*/
        ImageView radiationPicture = (ImageView) findViewById(R.id.radiation);
        int radiationimageResourse = getResources().getIdentifier("@drawable/nuclear",null, this.getPackageName());
        radiationPicture.setImageResource(radiationimageResourse);

        /*Picture to hazard*/
        ImageView hazardPicture = (ImageView) findViewById(R.id.hazard);
        int hazardimageResourse = getResources().getIdentifier("@drawable/biohazard",null, this.getPackageName());
        hazardPicture.setImageResource(hazardimageResourse);

        /*Picture to noise*/
        ImageView noisePicture = (ImageView) findViewById(R.id.noise);
        int noiseimageResourse = getResources().getIdentifier("@drawable/ear",null, this.getPackageName());
        noisePicture.setImageResource(noiseimageResourse);


        ImageView framerightPicture = (ImageView) findViewById(R.id.frameright2);
        int imageframerightResourse = getResources().getIdentifier("@drawable/frames",null, this.getPackageName());
        framerightPicture.setImageResource(imageframerightResourse);
    }
    public void addReport(View v){
        switch (v.getId()){
            case R.id.smell:
                TextView smellText = (TextView) findViewById(R.id.textView12);
                String smellPic = "smell";
                openAddReporClass(smellText,smellPic );
                break;
            case R.id.Smoke:
                TextView smokeText = (TextView) findViewById(R.id.textView18);
                String smokePic = "company";
                openAddReporClass(smokeText,smokePic);
                break;
            case R.id.trash:
                TextView trashText = (TextView) findViewById(R.id.textView26);
                String trashPic = "trash";
                openAddReporClass(trashText, trashPic);
                break;
            case R.id.radiation:
                TextView radiationText = (TextView) findViewById(R.id.textView29);
                String radiationPic = "nuclear";
                openAddReporClass(radiationText, radiationPic);
                break;
            case R.id.hazard:
                TextView hazardText = (TextView) findViewById(R.id.textView30);
                String hazardPic = "biohazard";
                openAddReporClass(hazardText, hazardPic);
                break;
            case R.id.noise:
                TextView noiseText = (TextView) findViewById(R.id.textView25);
                String noisePic = "ear";
                openAddReporClass(noiseText, noisePic);
                break;
        }
    }
    public void openAddReporClass(TextView textView, String name){
        String text = textView.getText().toString();
        Intent i = new Intent(this, AddReport.class);
        i.putExtra(TEXT,text);
        i.putExtra(TEXTIMAGE,name);
        i.putExtra(myCurrentLocation_latitude,latitude);
        i.putExtra(myCurrentLocation_longtitude,longtitude);
        startActivity(i);
    }
}