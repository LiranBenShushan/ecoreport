package com.example.myapplication;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

public class MyItem implements ClusterItem {

    private final LatLng position;
    private final String title;
    private final String snippet;
    private final E_Locations kindOfLocation;
    private final int NO2;
    private final int NOX;
    private final int CO;
    private final int O3;
    private final int SO2;
    private final int PM10;
    private final int PM2_5;
    private final int TotalValue;
    private final String Date;

    public MyItem(double lat, double lng, String title, String snippet, E_Locations kindOfLocation, int no2, int nox, int co, int o3, int so2, int pm10, int pm2_5, int totalValue, String date) {
        this.NO2 = no2;
        this.NOX = nox;
        this.CO = co;
        this.O3 = o3;
        this.SO2 = so2;
        this.PM10 = pm10;
        this.PM2_5 = pm2_5;
        this.TotalValue = totalValue;
        this.Date = date;
        this.position = new LatLng(lat, lng);
        this.title = title;
        this.snippet = snippet;
        this.kindOfLocation = kindOfLocation;
    }
    public String getDate() {
        return Date;
    }
    public LatLng getPosition() {
        return position;
    }

    public String getTitle() {
        return title;
    }

    public String getSnippet() {
        return snippet;
    }

    public E_Locations getKindOfLocation() {
        return kindOfLocation;
    }

    public int getTotalValue(){
        return TotalValue;
    }
}
