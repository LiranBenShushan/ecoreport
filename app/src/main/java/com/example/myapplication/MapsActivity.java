package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentActivity;
import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.navigation.NavigationView;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

public class MapsActivity extends FragmentActivity
        implements
        OnMapReadyCallback,
        ClusterManager.OnClusterClickListener<DropOnMapItem>,
        ClusterManager.OnClusterItemClickListener<DropOnMapItem>,
        ClusterManager.OnClusterItemInfoWindowClickListener<DropOnMapItem>, NavigationView.OnNavigationItemSelectedListener
{
    private DropOnMapItem clickedClusterItem;
    private static Location myCurrentLocation;
    private GoogleMap mMap;
    private boolean checkerLocation = true;
    private boolean legenedIsOn = false;
    private List markers = null;
    private ClusterManager<DropOnMapItem> clusterReportOnMap;
    private ArrayList<ReportItem> allReports = new ArrayList<>();
    private ArrayList<MyItem> allStations = new ArrayList<>();
    private MyItem currentStation;
    private boolean markLocation = false;
    private String report;
    private String snippet;
    private String time;
    private String category_report;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    public static final String STATION_NAME = "com.example.myapplication.example.STATION_NAME";
    public static final String User_NAME = "com.example.myapplication.example.User_NAME";
    public static final String myCurrentLocation_latitude = "com.example.myapplication.example.myCurrentLocation_latitude";
    public static final String myCurrentLocation_longtitude = "com.example.myapplication.example.myCurrentLocation_longtitude";
    public static final String report_from_marker = "com.example.myapplication.example.report_from_marker";
    public static final String report_snippet = "com.example.myapplication.example.report_snippet";
    public static final String report_category = "com.example.myapplication.example.report_category";
    public static final String report_type = "com.example.myapplication.example.report_type";
    public static final String TIME = "com.example.myapplication.example.TIME";

    public static final String weather = "com.example.myapplication.example.weather";
    public static final String time_weather = "com.example.myapplication.example.time_weather";
    public static final String temperture = "com.example.myapplication.example.temperture";
    public static ReportItem report_by_user;
    public static Marker report_by_user_Pin;


    Geocoder geo;
    TextView txtMarkers, userName;
    private DrawerLayout drawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.mapsmarkers);
        SupportMapFragment supportMapFragment = (SupportMapFragment)getSupportFragmentManager().findFragmentById(R.id.map);
        supportMapFragment.getMapAsync(this);

        final SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        createIconsForMainScreen();

        Intent intent = getIntent();
        if(intent != null){
            String startLocation = intent.getStringExtra(addReportDetails.TEXT);

            if(startLocation != null){
                if(startLocation.equals("StartLocation")) {
                    markLocation = true;
                    report = intent.getStringExtra(addReportDetails.REPORT);
                    snippet = intent.getStringExtra(addReportDetails.SNIPPET);
                    category_report = intent.getStringExtra(addReportDetails.CATEGORY);

//                    try {
//                        newMarkerFromReport();
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
                }
            }

        }

//        Toolbar toolbar = findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
        drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

    }

    public void loginScreen (View v){

        Intent i = new Intent(MapsActivity.this, LoginActivity.class);
        startActivity(i);
    }

    public void logOut (View v){
        Button loging = findViewById(R.id.login_out);
        loging.setText("יציאה");

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Intent i = null;
        switch (item.getItemId()) {
            case R.id.user_area:
                TextView userNameClient = findViewById(R.id.userName);
                if(!userNameClient.getText().toString().equals("אורח")) {
                    i = new Intent(MapsActivity.this, UserSection.class);
                    i.putExtra(User_NAME, userNameClient.getText().toString());
                    startActivity(i);
                }else{
                    Toast.makeText(this, "עלייך להיות רשום כדי להיכנס לאזור האישי", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.environmentalist:
                i = new Intent(MapsActivity.this, Environmentalists.class);
                startActivity(i);
                return true;
            case R.id.monitoring_stations:// clicked on דוחות תחנה
                i = new Intent(MapsActivity.this, MonitoringStation.class);
                startActivity(i);
                return true;
            case R.id.environment:// clicked on חדשות
                String url = "http://www.envihaifa.org.il/חדשות";
                i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
                return true;
            case R.id.report_near_user: //clicked on דיווחים בסביבתך
                i = new Intent(MapsActivity.this, ForumPage.class);

                i.putExtra(myCurrentLocation_latitude,String.valueOf(myCurrentLocation.getLatitude()));
                i.putExtra(myCurrentLocation_longtitude,String.valueOf(myCurrentLocation.getLongitude()));

                i.putExtra(report_from_marker,String.valueOf(report));
                i.putExtra(report_snippet,String.valueOf(snippet));
                i.putExtra(report_category,String.valueOf(category_report));
                i.putExtra(report_type,String.valueOf(report));
                i.putExtra(TIME,String.valueOf(time));
                Log.wtf("TIME", String.valueOf(time));
                startActivity(i);
                return true;
            case R.id.settings: //clicked on דיווחים בסביבתך
                return true;

        }

            drawer.closeDrawer(GravityCompat.START);

        return true;
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.getUiSettings().setCompassEnabled(true);
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                mMap.setMyLocationEnabled(true);
                mMap.setOnMyLocationChangeListener(myLocationChangeListener);

            }
        } else {
            mMap.setMyLocationEnabled(true);
        }
        geo = new Geocoder(MapsActivity.this, Locale.getDefault());

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

            @Override
            public void onMapClick(LatLng latLng) {
                findViewById(R.id.report_details).setVisibility(View.INVISIBLE);
                findViewById(R.id.report_type).setVisibility(View.INVISIBLE);
                findViewById(R.id.snippet_details_report).setVisibility(View.INVISIBLE);
                findViewById(R.id.distance_report).setVisibility(View.INVISIBLE);
                findViewById(R.id.address_report).setVisibility(View.INVISIBLE);
                findViewById(R.id.date_report).setVisibility(View.INVISIBLE);
                findViewById(R.id.confirmed_by_report).setVisibility(View.INVISIBLE);
                findViewById(R.id.dis_like_report).setVisibility(View.INVISIBLE);
                findViewById(R.id.like_report).setVisibility(View.INVISIBLE);
            }
        });


//        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
//            @Override
//            public void onInfoWindowClick(Marker marker) {
//                marker.hideInfoWindow();
////                Intent i = new Intent(MapsActivity.this, MonitoringStation.class);
////                startActivity(i);
//            }
//        });

        setUpClusterer();

        buildMenu();

    }

    public void buildMenu(){
        final ImageButton imageButton = findViewById(R.id.menu); // get your ImageButton from the XML here
        imageButton.setImageResource(getResources().getIdentifier("@drawable/menu",null, this.getPackageName()));


        final PopupMenu dropDownMenu = new PopupMenu(this, imageButton);

        imageButton.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("WrongConstant")
            @Override
            public void onClick(View v) {
                DrawerLayout navDrawer = findViewById(R.id.drawer_layout);
                // If the navigation drawer is not open then open it, if its already open then close it.
                if(!navDrawer.isDrawerOpen(Gravity.START)) navDrawer.openDrawer(Gravity.START);
                else navDrawer.closeDrawer(Gravity.END);
                Intent intent2 = getIntent();
                Log.wtf("intent2", String.valueOf(intent2));
                if(intent2 != null) {
                    TextView userNameClient = findViewById(R.id.userName);
                    Log.wtf("userNameClient", String.valueOf(userNameClient));
                    String user_mailFromGmailAccount = intent2.getStringExtra(LoginActivity.account_userName);
                    String user_mailFromRegistration = intent2.getStringExtra(RegisterActivity.account_userName);

                    if(user_mailFromGmailAccount != null || user_mailFromRegistration != null) {
                        userNameClient.setText(user_mailFromGmailAccount);
                        Button loging = findViewById(R.id.login_out);
                        loging.setText("יציאה");
                    }
                }
            }
        });


// if you want to be able to open the menu by dragging on the button:
        imageButton.setOnTouchListener(dropDownMenu.getDragToOpenListener());
    }


    public void  checkDistance(Location currentLocation){
        ImageView pollutionPicture = (ImageView) findViewById(R.id.imageView3);
        Location stationLocation = new Location(LocationManager.GPS_PROVIDER);
        stationLocation.setLatitude(allStations.get(0).getPosition().latitude);
        stationLocation.setLongitude(allStations.get(0).getPosition().longitude);
        currentStation = allStations.get(0);
        float minLocationStaionFromCurrent = currentLocation.distanceTo(stationLocation)/1000;
        float currentDistance;
        int currentTotalPollution = allStations.get(0).getTotalValue();

        for(MyItem station : allStations){

            stationLocation.setLatitude(station.getPosition().latitude);
            stationLocation.setLongitude(station.getPosition().longitude);

            currentDistance = currentLocation.distanceTo(stationLocation)/1000;
            if(currentDistance < minLocationStaionFromCurrent) {
                minLocationStaionFromCurrent = currentDistance;
                currentTotalPollution = station.getTotalValue();
                currentStation = station;
            }
        }

        TextView imageButton;
        imageButton = findViewById(R.id.textView8);
        if(minLocationStaionFromCurrent < 100) {
            imageButton.setText(String.valueOf(currentTotalPollution));

            if(100 >= currentTotalPollution && currentTotalPollution > 50)
                pollutionPicture.setImageResource(getResources().getIdentifier("@drawable/green",null, this.getPackageName()));
            else if(50 >= currentTotalPollution && currentTotalPollution > -1)
                pollutionPicture.setImageResource(getResources().getIdentifier("@drawable/orange",null, this.getPackageName()));
            else if(-1 >= currentTotalPollution && currentTotalPollution > -200)
                pollutionPicture.setImageResource(getResources().getIdentifier("@drawable/red",null, this.getPackageName()));

        }else{
            imageButton.setText("אין תחנת ניטור בסביבתך");

        }

    }

    public void sendToStation(View v){
        if(currentStation != null){
            String text = currentStation.getTitle();
            Intent i = new Intent(this, MonitoringStation.class);
            i.putExtra(STATION_NAME,text);

            startActivity(i);
        }
    }

    public void newMarkerFromReport() throws IOException {//I checked and its working. need to this how to pass the location

        String currentDate = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(new Date());
        String currentTime = new SimpleDateFormat("HH:mm", Locale.getDefault()).format(new Date());

        LatLng coordinate = new LatLng(myCurrentLocation.getLatitude(), myCurrentLocation.getLongitude());
        E_Locations categoryReport;

        if(category_report.equals("smell"))
            categoryReport = E_Locations.Smell_Report;
        else if(category_report.equals("ear"))
            categoryReport = E_Locations.Sound_Report;
        else
            categoryReport = E_Locations.Sound_Report;

        List<Address> address = geo.getFromLocation(coordinate.latitude, coordinate.longitude, 1);
        report_by_user_Pin = mMap.addMarker(new MarkerOptions()
                .position(coordinate)
                .title("דיווח: " + report)
                .snippet(snippet + "תאריך דיווח:" + currentDate + " שעת דיווח: "+ currentTime)
                .icon(categoryReport.equals(E_Locations.Sound_Report)? BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE): BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
        time = currentDate + " | " + currentTime;

    }




    private void setUpClusterer() {
        // Position the map.
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(32.85193, 35.080065), 10));

        // Initialize the manager with the context and the map.
        clusterReportOnMap = new ClusterManager<>(this, mMap);
        clusterReportOnMap.setRenderer(new MarkerClusterRenderer(this, mMap, clusterReportOnMap));
        clusterReportOnMap.setOnClusterClickListener(this);
        clusterReportOnMap.setOnClusterItemInfoWindowClickListener(this);

        // Add cluster items (markers) to the cluster manager.
        clusterReportOnMap
                .setOnClusterItemClickListener(new ClusterManager.OnClusterItemClickListener<DropOnMapItem>() {
                    @Override
                    public boolean onClusterItemClick(DropOnMapItem item) {
                        clickedClusterItem = item;
                        return false;
                    }
                });

        mMap.setOnCameraIdleListener(clusterReportOnMap);
        mMap.setOnMarkerClickListener(clusterReportOnMap);

        addItems();

        if(markLocation) {
            try {
                newMarkerFromReport();
            } catch (IOException e) {
                e.printStackTrace();
            }
            markLocation = false;
        }

        clusterReportOnMap.getMarkerCollection().setInfoWindowAdapter(
                new CustomClusterMarkerInfoWindowView(getApplicationContext()));

    }


    private void addItems() {
        readFromCSV();
    }

    @Override
    public boolean onClusterClick(Cluster<DropOnMapItem> cluster) {

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(cluster.getPosition(),13));
        return false;
    }

    @Override
    public boolean onClusterItemClick(DropOnMapItem item) {

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(item.getPosition(),10));

        return false;
    }


    @Override
    public void onClusterItemInfoWindowClick(DropOnMapItem item) {

        if(item.getKindOfLocation().equals(E_Locations.Igud_Station)){
            //Cluster item InfoWindow clicked, set title as action
            String text = item.getName();
            Intent i = new Intent(this, MonitoringStation.class);
            i.putExtra(STATION_NAME,text);
            startActivity(i);
        }
    }


    private GoogleMap.OnMyLocationChangeListener myLocationChangeListener = new GoogleMap.OnMyLocationChangeListener() {
        public void onMyLocationChange(Location location) {
            myCurrentLocation = new Location(location);
            LatLng loc = new LatLng(location.getLatitude(), location.getLongitude());
            if (mMap != null && checkerLocation) {
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(loc, 14));
                checkDistance(myCurrentLocation);

                checkerLocation = false;
            }
        }
    };


    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSIONS_REQUEST_LOCATION);
            return false;
        } else {
            return true;
        }
    }

    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay!
                    if (ActivityCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        mMap.setMyLocationEnabled(true);
                    }
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(this, "permission to current location is denied", Toast.LENGTH_LONG).show();
                }
                return;
            }
        }
    }


    public void readFromCSV(){
        InputStream is = getResources().openRawResource(R.raw.stations);
        BufferedReader reader = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
        String line = "";
        try{
            // step headers
            reader.readLine();
            while( (line = reader.readLine()) != null ){
                Log.d("MyActivity","Line: " + line);
                // Split by ','
                String[] token = line.split(",");
                //read the data
                MyItem location = new MyItem(Double.valueOf(token[0]), Double.valueOf(token[1]), token[2], token[3],token[4].equals(E_Locations.Igud_Station) ? E_Locations.Igud_Station: E_Locations.Igud_Station, Integer.parseInt(token[5]),Integer.parseInt(token[6]), Integer.parseInt(token[7]), Integer.parseInt(token[8]), Integer.parseInt(token[9]), Integer.parseInt(token[10]), Integer.parseInt(token[11]),Integer.parseInt(token[12]), token[13]);
                allStations.add(location);

                DropOnMapItem addToMap = new DropOnMapItem(location.getTitle(), location.getPosition().latitude, location.getPosition().longitude, location.getSnippet(), location.getKindOfLocation(), location.getDate() + "", "", null);
                clusterReportOnMap.addItem(addToMap);
            }

        }catch(IOException e){
            Log.wtf("MyActivity", "The specified file was not found");
            e.printStackTrace();
        }

        InputStream reportCSV = getResources().openRawResource(R.raw.reports);
        BufferedReader readerReportCSV = new BufferedReader(new InputStreamReader(reportCSV, Charset.forName("UTF-8")));
        String lineReportCSV = "";
        try{
            // step headers
            readerReportCSV.readLine();
            while( (lineReportCSV = readerReportCSV.readLine()) != null ) {
                String[] tokenReport = lineReportCSV.split(",");
                ReportItem report = new ReportItem(tokenReport[0].equals(E_Locations.Smell_Report.toString()) ? E_Locations.Smell_Report: E_Locations.Sound_Report, tokenReport[1], tokenReport[2], Double.valueOf(tokenReport[3]), Double.valueOf(tokenReport[4]), Integer.valueOf(tokenReport[5]), tokenReport[6], tokenReport[7], confirmedBy(tokenReport[8]));
                allReports.add(report);
                DropOnMapItem addToMap = new DropOnMapItem("דיווח: " +report.getType(), report.getPosition().latitude, report.getPosition().longitude, report.getSnippet(), report.getCategory(), report.getReportTime() + "", report.getUserMail(),report.getConfirmedBy());
                clusterReportOnMap.addItem(addToMap);
            }

        }catch(IOException e){
            Log.wtf("MyActivity", "The specified file was not found");
            e.printStackTrace();
        }



    }
    public E_Levels confirmedBy (String name){

        if (name.equals(E_Levels.EnvironmentFlower.toString())){
            return E_Levels.EnvironmentFlower;
        }
        if (name.equals(E_Levels.EnviromentLoyal.toString())){
            return E_Levels.EnviromentLoyal;
        }
        if (name.equals(E_Levels.SeniorEnvironmentalist.toString())){
            return E_Levels.SeniorEnvironmentalist;
        }
            return E_Levels.waiting_for_confirmation;

    }

    public Location getLocation() {
        return myCurrentLocation;
    }

    public void make_report_panel(Marker marker){

            marker.hideInfoWindow();

            ImageView legenedContainer = findViewById(R.id.report_details);
            legenedContainer.setVisibility(View.VISIBLE);

            TextView report_type = findViewById(R.id.report_type);
            report_type.setVisibility(View.VISIBLE);
            report_type.setText(marker.getTitle());

            TextView snippet_details_report = findViewById(R.id.snippet_details_report);
            snippet_details_report.setVisibility(View.VISIBLE);


            TextView distance_report = findViewById(R.id.distance_report);
            distance_report.setVisibility(View.VISIBLE);

            TextView address_report = findViewById(R.id.address_report);
            address_report.setVisibility(View.VISIBLE);

            TextView date_report = findViewById(R.id.date_report);
            date_report.setVisibility(View.VISIBLE);

            ImageView confirmed_by_report = findViewById(R.id.confirmed_by_report);
            confirmed_by_report.setVisibility(View.VISIBLE);


            /*dislike icons pictures */
            ImageButton disLike = findViewById(R.id.dis_like_report);
            disLike.setVisibility(View.VISIBLE);
            int imageDisLike = getResources().getIdentifier("@drawable/dislike",null, MapsActivity.this.getPackageName());
            disLike.setImageResource(imageDisLike);

            /*like icons pictures */
            ImageButton like = findViewById(R.id.like_report);
            like.setVisibility(View.VISIBLE);
            int imageLike = getResources().getIdentifier("@drawable/like",null, MapsActivity.this.getPackageName());
            like.setImageResource(imageLike);

            if(marker != null && marker.getTitle() != null ){
                for (ReportItem item : allReports){
                    if (marker.getTitle().equals("דיווח: " + item.getType())) {
                        if (item.getConfirmedBy().toString().equals(E_Levels.EnvironmentFlower.toString()))
                            confirmed_by_report.setImageResource(getResources().getIdentifier("@drawable/environment_flower", null, MapsActivity.this.getPackageName()));
                        else if (item.getConfirmedBy().toString().equals(E_Levels.EnviromentLoyal.toString()))
                            confirmed_by_report.setImageResource(getResources().getIdentifier("@drawable/environment_trustee", null, MapsActivity.this.getPackageName()));
                        else if (item.getConfirmedBy().toString().equals(E_Levels.SeniorEnvironmentalist.toString()))
                            confirmed_by_report.setImageResource(getResources().getIdentifier("@drawable/environment_guard_senior", null, MapsActivity.this.getPackageName()));
                        else if (item.getConfirmedBy().toString().equals(E_Levels.waiting_for_confirmation.toString()))
                            confirmed_by_report.setImageResource(getResources().getIdentifier("@drawable/waiting_for_confirmation", null, MapsActivity.this.getPackageName()));

                        snippet_details_report.setText(item.getDescription());

                        Location stationLocation = new Location(LocationManager.GPS_PROVIDER);
                        stationLocation.setLatitude(item.getPosition().latitude);
                        stationLocation.setLongitude(item.getPosition().longitude);
                        float distance = myCurrentLocation.distanceTo(stationLocation)/1000;
                        if(distance > 1)
                            distance_report.setText("מרחק ממיקומך: " +new DecimalFormat("##.##").format(distance) + " ק''מ");
                        else
                            distance_report.setText("מרחק ממיקומך: " +new DecimalFormat("##.##").format(distance*1000) + " מטר");


                        List<Address> address = null;
                        try {
                            address = geo.getFromLocation(item.getPosition().latitude, item.getPosition().longitude, 1);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        address_report.setText(address.get(0).getAddressLine(0));

                        date_report.setText("תאריך דיווח: " +item.getReportTime());

                    }
                }

                for (MyItem station : allStations){
                    if (marker.getTitle().equals(station.getTitle())) {
                        confirmed_by_report.setImageResource(0);
                        snippet_details_report.setText("רמת זיהום נוכחית: " + String.valueOf(station.getTotalValue()));
                        List<Address> address = null;
                        try {
                            address = geo.getFromLocation(station.getPosition().latitude, station.getPosition().longitude, 1);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        address_report.setText(address.get(0).getAddressLine(0));
                        date_report.setText("תאריך עדכון: " +station.getDate());

                        like.setVisibility(View.INVISIBLE);
                        disLike.setVisibility(View.INVISIBLE);
                    }
                }
            }




    }



    public class CustomClusterMarkerInfoWindowView implements GoogleMap.InfoWindowAdapter{
        private Context context;

        public CustomClusterMarkerInfoWindowView(Context context) {
            this.context = context.getApplicationContext();

        }

        @Override
        public View getInfoWindow(Marker arg0) {
            return null;
        }

        @Override
        public View getInfoContents(Marker marker) {
            marker.hideInfoWindow();

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View v =  inflater.inflate(R.layout.map_marker_info_window, null);

            make_report_panel(marker);
            return v;
        }


    }















    public void createIconsForMainScreen(){

        /*report icons pictures */
        final ImageButton reportButton = findViewById(R.id.button3); // get your ImageButton from the XML here
        int imagereport = getResources().getIdentifier("@drawable/locationgreen",null, this.getPackageName());
        reportButton.setImageResource(imagereport);

        /*legend icons pictures */
        ImageButton legendButton1 = findViewById(R.id.legenedButton); // get your ImageButton from the XML here
        int legendButtonInt = getResources().getIdentifier("@drawable/down_arrow",null, this.getPackageName());
        legendButton1.setImageResource(legendButtonInt);

        /*legend icons pictures */
        ImageView orangeMarkerButton1 = findViewById(R.id.legenedContainer); // get your ImageButton from the XML here
//        orangeMarkerButton1.setImageResource(getResources().getIdentifier("@drawable/bell", null, MapsActivity.this.getPackageName()));

//        /*Picture to frameright icon*/
//        ImageView framerightPicture = (ImageView) findViewById(R.id.frameright);
//        int imageframerightResourse = getResources().getIdentifier("@drawable/frames",null, this.getPackageName());
//        framerightPicture.setImageResource(imageframerightResourse);

    }

    public void legenedOpen(View v){
        ImageButton legendButton1 = findViewById(R.id.legenedButton); // get your ImageButton from the XML here
        ImageView legenedContainer = findViewById(R.id.legenedContainer);


        ImageButton orangeMarkerPicture = (ImageButton) findViewById(R.id.orangeMarker);
        ImageButton satellitePicture = (ImageButton) findViewById(R.id.stationMarker);
        ImageButton blueMarkerPicture = (ImageButton) findViewById(R.id.blueMarker);
        ImageButton redMarkerPicture = (ImageButton) findViewById(R.id.redMarker);

        TextView textStation = findViewById(R.id.textStation);
        TextView textOrange = findViewById(R.id.textOrange);
        TextView textBlue = findViewById(R.id.textBlue);
        TextView textRed = findViewById(R.id.textRed);
        if(!legenedIsOn){

            legenedIsOn = true;
            legenedContainer.setVisibility(View.VISIBLE);
            int legendButtonInt = getResources().getIdentifier("@drawable/up_arrow",null, this.getPackageName());
            legendButton1.setImageResource(legendButtonInt);

            /*Picture to orangeMarker icon*/
            orangeMarkerPicture.setVisibility(View.VISIBLE);
            int orangeInt = getResources().getIdentifier("@drawable/orange_marker",null, this.getPackageName());
            orangeMarkerPicture.setImageResource(orangeInt);

            /*Picture to orangeMarker icon*/
            blueMarkerPicture.setVisibility(View.VISIBLE);
            int blueMarkerInt = getResources().getIdentifier("@drawable/blue_marker",null, this.getPackageName());
            blueMarkerPicture.setImageResource(blueMarkerInt);

            /*Picture to redMarker icon*/
            redMarkerPicture.setVisibility(View.VISIBLE);
            int redMarkerInt = getResources().getIdentifier("@drawable/red_marker",null, this.getPackageName());
            redMarkerPicture.setImageResource(redMarkerInt);

            /*Picture to satellite icon*/
            satellitePicture.setVisibility(View.VISIBLE);
            int satelliteInt = getResources().getIdentifier("@drawable/satellite",null, this.getPackageName());
            satellitePicture.setImageResource(satelliteInt);


            textStation.setVisibility(View.VISIBLE);
            textOrange.setVisibility(View.VISIBLE);
            textBlue.setVisibility(View.VISIBLE);
            textRed.setVisibility(View.VISIBLE);
        }else{
            legenedIsOn = false;
            legenedContainer.setVisibility(View.INVISIBLE);
            int legendButtonInt = getResources().getIdentifier("@drawable/down_arrow",null, this.getPackageName());
            legendButton1.setImageResource(legendButtonInt);

            blueMarkerPicture.setVisibility(View.INVISIBLE);
            orangeMarkerPicture.setVisibility(View.INVISIBLE);
            redMarkerPicture.setVisibility(View.INVISIBLE);
            satellitePicture.setVisibility(View.INVISIBLE);

            textStation.setVisibility(View.INVISIBLE);
            textOrange.setVisibility(View.INVISIBLE);
            textBlue.setVisibility(View.INVISIBLE);
            textRed.setVisibility(View.INVISIBLE);
        }
    }

    public void weatherWebSite(View v) throws IOException {
        String url = "https://weather.com/weather/today/l/32.52,34.95?par=google&temp=c";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }




    public void reportFromMap(View v) throws IOException, TransformerException, ParserConfigurationException {
        Intent i = new Intent(this, ReportFromMap.class);
        i.putExtra(myCurrentLocation_latitude,String.valueOf(myCurrentLocation.getLatitude()));
        i.putExtra(myCurrentLocation_longtitude,String.valueOf(myCurrentLocation.getLongitude()));
        startActivity(i);
    }

}