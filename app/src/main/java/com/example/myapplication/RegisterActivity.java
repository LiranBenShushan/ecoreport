package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class RegisterActivity extends AppCompatActivity {
    public static final String account_userName = "com.example.myapplication.example.account";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }

        TextView btn=findViewById(R.id.alreadyHaveAccount);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(RegisterActivity.this,LoginActivity.class));
            }
        });
    }

    public void registerCompleted (View v){
        TextView userName = findViewById(R.id.inputUsername);
        Intent googleAccountIntent = new Intent(this, MapsActivity.class);

        googleAccountIntent.putExtra(account_userName,userName.getText().toString());
        startActivity(googleAccountIntent);
    }
}
