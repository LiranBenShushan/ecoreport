package com.example.myapplication;

import androidx.annotation.Nullable;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

public class DropOnMapItem implements ClusterItem {
    private final String nameItem;
    private final LatLng position;
    private final String snippet;
    private final E_Locations kindOfLocation;
    private final String dateOfItem;
    private final String e_mail;
    private final E_Levels confirmedBy;

    public DropOnMapItem(String nameItem, double lat, double lng, String snippet, E_Locations kindOfLocation, String dateOfItem, String e_mail, E_Levels confirmedBy) {
        this.nameItem = nameItem;
        this.dateOfItem = dateOfItem;
        this.position = new LatLng(lat, lng);
        this.snippet = snippet;
        this.kindOfLocation = kindOfLocation;
        this.e_mail = e_mail;
        this.confirmedBy = confirmedBy;
    }

    public LatLng getPosition() {
        return position;
    }

    @Nullable
    @Override
    public String getTitle() {
        return nameItem;
    }

    public String getDateOfItem() {
        return dateOfItem;
    }
    public String getName() {
        return nameItem;
    }
    public String getSnippet() {
        return snippet;
    }
    public String getE_mail() {
        return e_mail;
    }
    public E_Locations getKindOfLocation() {
        return kindOfLocation;
    }
    public E_Levels getConfirmedBy() {
        return confirmedBy;
    }

}
