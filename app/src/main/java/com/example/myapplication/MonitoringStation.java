package com.example.myapplication;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.gms.maps.model.Marker;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;

import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapplication.ui.main.SectionsPagerAdapter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

public class MonitoringStation extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    private ArrayList<MyItem> allStationDetails = new ArrayList<>();
    private boolean stationFromMap = false;
    public static final String station_address = "com.example.myapplication.example.station_address";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_monitoring_station);
        SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(this, getSupportFragmentManager());
        ViewPager viewPager = findViewById(R.id.view_pager);
        viewPager.setAdapter(sectionsPagerAdapter);
        TabLayout tabs = findViewById(R.id.tabs);
        tabs.setupWithViewPager(viewPager);
        FloatingActionButton fab = findViewById(R.id.fab);


        viewPager.setCurrentItem(2);



        Spinner spinner = (Spinner) findViewById(R.id.spinnerStations);

        // Spinner click listener
        spinner.setOnItemSelectedListener(this);

        // Spinner Drop down elements
        List<String> stations = new ArrayList<String>();
        stations.add("חיפה - איגוד");
        stations.add("אחוזה");
        stations.add("אחוזה תחבורתית");
        stations.add("דליית אל כרמל");
        stations.add("הדר");
        stations.add("כפר חסידים");
        stations.add("נווה שאנן");
        stations.add("נשר");
        stations.add("ק.ביאליק עופרים");
        stations.add("ק.מוצקין בגין");
        stations.add("קריית אתא");
        stations.add("קריית בנימין");
        stations.add("קריית חיים מערבית");
        stations.add("קריית טבעון");
        stations.add("קריית ים");
        stations.add("שפרינצק");
        stations.add("תחנה ניידת איגוד");


        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, stations);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinner.setAdapter(dataAdapter);



        Intent intent = getIntent();
        if(intent != null){
            String text = intent.getStringExtra(MapsActivity.STATION_NAME);
            if(text != null){
                TextView stationLocation = findViewById(R.id.location_station);
                if(text.equals("תחנת ניטור שפרינצק")) {
                    spinner.setSelection(15);
//                    stationLocation.setText("כתובת: דרך צרפת 79 - חיפה (על הקרקע ליד בית ספר רמות)");

                }
                else if(text.equals("תחנת ניטור חיפה - הדר")) {
                    spinner.setSelection(4);
//                    stationLocation.setText("כתובת: רחוב בלפור 2 - חיפה (צומת בית הקרנות)");
                }
                else if(text.equals("תחנת ניטור קריית אתא")) {
                    spinner.setSelection(10);
//                    stationLocation.setText("כתובת: בית ספר מקיף רוגוזין, רחוב הוגו מולר 13 - קריית אתא (על הגג)");
                }
                else if(text.equals("תחנת ניטור דליית אל כרמל")) {
//                    stationLocation.setText("כתובת: שטח מתקן איסוף שפכים");
                    spinner.setSelection(3);
                }
            }

        }



    }



    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        // On selecting a spinner item
        String item = parent.getItemAtPosition(position).toString();

        // Showing selected spinner item
        Toast.makeText(parent.getContext(), "Selected: " + item, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}