package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class Report_Details extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_details);

        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }

        Intent intent = getIntent();
        TextView title = findViewById(R.id.report_name);
        title.setText("דיווח: " + intent.getStringExtra(ForumPage.reportType));
        String confirmedBy =intent.getStringExtra(ForumPage.confirmedBy);
        String time = intent.getStringExtra(ForumPage.time);

        ImageView environment_flower_rank3 = findViewById(R.id.environment_flower_rank3);
        ImageView environment_trustee_rank2 = findViewById(R.id.environment_trustee_rank2);
        ImageView environment_guard_senior_rank2 = findViewById(R.id.environment_guard_senior_rank2);

        ImageView flower_done = findViewById(R.id.flower_done);
        ImageView trustee_done = findViewById(R.id.trustee_done);
        ImageView senior_done = findViewById(R.id.senior_done);
        ImageView done_done =  findViewById(R.id.done_done);


        TextView last_response_timer = findViewById(R.id.user_name29);

        TextView report_clock = findViewById(R.id.report_clock);
        TextView flower_clock = findViewById(R.id.flower_clock);
        TextView trustee_clock = findViewById(R.id.trustee_clock);
        TextView senior_clock = findViewById(R.id.senior_clock);
        TextView done_clock = findViewById(R.id.done_clock);

        if(confirmedBy.equals(E_Levels.EnvironmentFlower.toString())){
            environment_flower_rank3.setImageResource(getResources().getIdentifier("@drawable/environment_flower",null, this.getPackageName()));
            environment_trustee_rank2.setImageResource(getResources().getIdentifier("@drawable/environment_trustee_black",null, this.getPackageName()));
            environment_guard_senior_rank2.setImageResource(getResources().getIdentifier("@drawable/environment_guard_senior_black",null, this.getPackageName()));

            report_clock.setText(time);
            last_response_timer.setText(time);
            flower_done.setVisibility(View.VISIBLE);
            flower_clock.setVisibility(View.VISIBLE);

        }
        else if(confirmedBy.equals(E_Levels.EnviromentLoyal.toString())){
            environment_flower_rank3.setImageResource(getResources().getIdentifier("@drawable/environment_flower",null, this.getPackageName()));
            environment_trustee_rank2.setImageResource(getResources().getIdentifier("@drawable/environment_trustee",null, this.getPackageName()));
            environment_guard_senior_rank2.setImageResource(getResources().getIdentifier("@drawable/environment_guard_senior_black",null, this.getPackageName()));
            flower_done.setVisibility(View.VISIBLE);
            trustee_done.setVisibility(View.VISIBLE);

            last_response_timer.setText(time);
            report_clock.setText(time);
            flower_clock.setVisibility(View.VISIBLE);
            flower_clock.setText(time);
            trustee_clock.setVisibility(View.VISIBLE);
            trustee_clock.setText(time);
        }else if(confirmedBy.equals(E_Levels.SeniorEnvironmentalist.toString())){
            environment_flower_rank3.setImageResource(getResources().getIdentifier("@drawable/environment_flower",null, this.getPackageName()));
            environment_trustee_rank2.setImageResource(getResources().getIdentifier("@drawable/environment_trustee",null, this.getPackageName()));
            environment_guard_senior_rank2.setImageResource(getResources().getIdentifier("@drawable/environment_guard_senior",null, this.getPackageName()));
            flower_done.setVisibility(View.VISIBLE);
            trustee_done.setVisibility(View.VISIBLE);
            senior_done.setVisibility(View.VISIBLE);
            done_done.setVisibility(View.VISIBLE);
            last_response_timer.setText(time);
            report_clock.setText(time);
            flower_clock.setVisibility(View.VISIBLE);
            flower_clock.setText(time);
            trustee_clock.setVisibility(View.VISIBLE);
            trustee_clock.setText(time);
            senior_clock.setVisibility(View.VISIBLE);
            senior_clock.setText(time);
            done_clock.setVisibility(View.VISIBLE);
            done_clock.setText(time);

        }else if (confirmedBy.equals(E_Levels.waiting_for_confirmation.toString())){
            environment_flower_rank3.setImageResource(getResources().getIdentifier("@drawable/environment_flower_black",null, this.getPackageName()));
            environment_trustee_rank2.setImageResource(getResources().getIdentifier("@drawable/environment_trustee_black",null, this.getPackageName()));
            environment_guard_senior_rank2.setImageResource(getResources().getIdentifier("@drawable/environment_guard_senior_black",null, this.getPackageName()));
            last_response_timer.setText(time);
            report_clock.setText(time);
        }
        String location =intent.getStringExtra(ForumPage.location);
        TextView address = findViewById(R.id.address);
        address.setText(location);

        String snippet =intent.getStringExtra(ForumPage.snippet__REPORT);
        Log.wtf("found snippet", String.valueOf(snippet));
        TextView snippet_report = findViewById(R.id.snippet_report);
        snippet_report.setText(snippet);

    }

    public E_Levels confirmedBy (String name){


        if (name.equals(E_Levels.EnvironmentFlower.toString())){
            return E_Levels.EnvironmentFlower;
        }
        if (name.equals(E_Levels.EnviromentLoyal.toString())){
            return E_Levels.EnviromentLoyal;
        }
        if (name.equals(E_Levels.SeniorEnvironmentalist.toString())){
            return E_Levels.SeniorEnvironmentalist;
        }
        return E_Levels.waiting_for_confirmation;


    }
}