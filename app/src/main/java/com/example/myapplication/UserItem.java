package com.example.myapplication;

import androidx.annotation.Nullable;

import com.google.android.gms.maps.model.LatLng;

import java.util.Date;

public class UserItem {
    private final String userMail;
    private final E_Titles kidomet;
    private final String firstName;
    private final String lastName;
    private final String phone;
    private final String address;
    private final E_Levels level;
    private final Date birthDate;
    private final Date signedUpDate;
    private final Date lastLogInDate;

    public UserItem(String userMail, E_Titles kidomet, String firstName, String lastName, String phone, String address, E_Levels level, Date birthDate, Date signedUpDate, Date lastLogInDate) {
        this.userMail = userMail;
        this.kidomet = kidomet;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phone = phone;
        this.address = address;
        this.level = level;
        this.birthDate = birthDate;
        this.signedUpDate = signedUpDate;
        this.lastLogInDate = lastLogInDate;
    }

    public String getUserMail() {
        return userMail;
    }

    public E_Titles getKidomet() {
        return kidomet;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPhone() {
        return phone;
    }

    public String getAddress() {
        return address;
    }

    public E_Levels getLevel() {
        return level;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public Date getSignedUpDate() {
        return signedUpDate;
    }

    public Date getLastLogInDate() {
        return lastLogInDate;
    }
}
