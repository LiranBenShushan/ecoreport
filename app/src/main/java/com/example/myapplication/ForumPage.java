package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class ForumPage extends AppCompatActivity {
    private ArrayList<ReportItem> allReports = new ArrayList<>();

    public static final String reportType = "com.example.myapplication.example.reportType";
    public static final String confirmedBy = "com.example.myapplication.example.confirmedBy";
    public static final String location = "com.example.myapplication.example.location";
    public static final String snippet__REPORT = "com.example.myapplication.example.snippet__REPORT";
    public static final String time = "com.example.myapplication.example.snippet";
    Geocoder geo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forum_page);
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }
        geo = new Geocoder(ForumPage.this, Locale.getDefault());
        readFromCSV();

        Intent intent = getIntent();
        String category = intent.getStringExtra(MapsActivity.report_category);
        String snippet = intent.getStringExtra(MapsActivity.report_snippet);
        if(snippet.equals("null"))
            snippet = "אין מידע נוסף";
        String report_type = intent.getStringExtra(MapsActivity.report_type);
        if(report_type.equals("null"))
            report_type = "סוג דיווח: מוזיקה רועשת";


        String time_report = intent.getStringExtra(MapsActivity.TIME);
        if(time_report.equals("null")){
            String currentDate = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(new Date());
            String currentTime = new SimpleDateFormat("HH:mm", Locale.getDefault()).format(new Date());
            time_report = currentDate + " | " + currentTime;
        }

        TextView time = findViewById(R.id.textView99);
        TextView title = findViewById(R.id.report_name);
        String myCurrentLocation_latitude = intent.getStringExtra(MapsActivity.myCurrentLocation_latitude);
        String myCurrentLocation_longtitude = intent.getStringExtra(MapsActivity.myCurrentLocation_longtitude);


        Location currentLocation = new Location(LocationManager.GPS_PROVIDER);
        currentLocation.setLatitude(Double.parseDouble(myCurrentLocation_latitude));
        currentLocation.setLongitude(Double.parseDouble(myCurrentLocation_longtitude));

        ReportItem report = new ReportItem(category.equals(E_Locations.Smell_Report.toString()) ? E_Locations.Smell_Report: E_Locations.Sound_Report, report_type, "koresh.roi", Double.parseDouble(myCurrentLocation_latitude) ,Double.parseDouble(myCurrentLocation_longtitude), 2, snippet, time_report, E_Levels.waiting_for_confirmation);
        allReports.add(report);

        TextView report_1 = findViewById(R.id.textView96);
        time.setText(report.getReportTime());
        TextView report_kind = findViewById(R.id.textView97);
        if(category.equals("smell"))
            report_kind.setText("סוג דיווח: ריח");
        else
            report_kind.setText("סוג דיווח: רעש");
        report_1.setText("דיווח: " + report.getType());

        TextView destination_report_1 = findViewById(R.id.destination_report_1);
        if(("דיווח: " +report.getType()).equals(report_1.getText().toString())){
            destination_report_1.setText(("מרחק ממיקומך: " +new DecimalFormat("##.##").format(currentLocation.distanceTo(currentLocation)) + " מטר"));
        }



        Location reportLocation = new Location(LocationManager.GPS_PROVIDER);

        Log.wtf("location", myCurrentLocation_latitude);

        TextView report_2 = findViewById(R.id.textView75);
        TextView report_2_time = findViewById(R.id.textView77);
        TextView report_3 = findViewById(R.id.textView79);
        TextView report_3_time = findViewById(R.id.textView81);
        TextView report_4 = findViewById(R.id.textView83);
        TextView report_4_time = findViewById(R.id.textView85);

        TextView destination_report_2 = findViewById(R.id.destination_report_2);
        TextView destination_report_3 = findViewById(R.id.destination_report_3);
        TextView destination_report_4 = findViewById(R.id.destination_report_4);
        for (ReportItem ri : allReports){
            reportLocation.setLatitude(ri.getPosition().latitude);
            reportLocation.setLongitude(ri.getPosition().longitude);
            double distance = currentLocation.distanceTo(reportLocation)/1000;
            String text_distance = null;
            if(distance > 1)
                text_distance = ("מרחק ממיקומך: " +new DecimalFormat("##.##").format(distance) + " ק''מ");
            else
                text_distance = ("מרחק ממיקומך: " +new DecimalFormat("##.##").format(distance*1000) + " מטר");

            if(("דיווח: " +ri.getType()).equals(report_2.getText().toString())){
                destination_report_2.setText(text_distance);
                report_2_time.setText(ri.getReportTime());
            }
            if(("דיווח: " +ri.getType()).equals(report_3.getText().toString())){
                destination_report_3.setText(text_distance);
                report_3_time.setText(ri.getReportTime());
            }
            if(("דיווח: " +ri.getType()).equals(report_4.getText().toString())){
                destination_report_4.setText(text_distance);
                report_4_time.setText(ri.getReportTime());
            }
        }
    }

    public void chooseReport1(View v) throws IOException {
        TextView myText = findViewById(R.id.textView96);
        reportDetails(myText);
    }
    public void chooseReport2(View v) throws IOException {
        TextView myText = findViewById(R.id.textView75);
        reportDetails(myText);
    }
    public void chooseReport3(View v) throws IOException {
        TextView myText = findViewById(R.id.textView79);
        reportDetails(myText);
    }
    public void chooseReport4(View v) throws IOException {
        TextView myText = findViewById(R.id.textView83);
        reportDetails(myText);
    }

    public void reportDetails(TextView myText) throws IOException {
        Intent i = new Intent(ForumPage.this, Report_Details.class);
        ReportItem chosen = null;
        Log.wtf("found reportItem", String.valueOf(myText.getText().toString()));
        for (ReportItem ri : allReports){
            if(("דיווח: " +ri.getType()).equals(myText.getText().toString())){
                chosen = ri;
                Log.wtf("found reportItem", String.valueOf(chosen.getType()));
            }
        }
        if (chosen !=null){
            Log.wtf("found getSnippet", String.valueOf(chosen.getSnippet()));
            Log.wtf("found getDescription", String.valueOf(chosen.getDescription()));
            i.putExtra(reportType, chosen.getType());
            i.putExtra(confirmedBy, chosen.getConfirmedBy().toString());
            List<Address> address = geo.getFromLocation(chosen.getPosition().latitude, chosen.getPosition().longitude, 1);
            i.putExtra(location, address.get(0).getAddressLine(0));
            i.putExtra(snippet__REPORT, chosen.getDescription());
            i.putExtra(time, chosen.getReportTime());

            startActivity(i);
        }else
            Log.wtf("reportItem is null", String.valueOf(chosen));
    }


    public void readFromCSV(){
         InputStream reportCSV = getResources().openRawResource(R.raw.reports);
        BufferedReader readerReportCSV = new BufferedReader(new InputStreamReader(reportCSV, Charset.forName("UTF-8")));
        String lineReportCSV = "";
        try{
            // step headers
            readerReportCSV.readLine();
            while( (lineReportCSV = readerReportCSV.readLine()) != null ) {
                String[] tokenReport = lineReportCSV.split(",");
                ReportItem report = new ReportItem(tokenReport[0].equals(E_Locations.Smell_Report.toString()) ? E_Locations.Smell_Report: E_Locations.Sound_Report, tokenReport[1], tokenReport[2], Double.valueOf(tokenReport[3]), Double.valueOf(tokenReport[4]), Integer.valueOf(tokenReport[5]), tokenReport[6], tokenReport[7], confirmedBy(tokenReport[8]));
                allReports.add(report);
            }

        }catch(IOException e){
            Log.wtf("MyActivity", "The specified file was not found");
            e.printStackTrace();
        }



    }
    public E_Levels confirmedBy (String name){

        if (name.equals(E_Levels.EnvironmentFlower.toString())){
            return E_Levels.EnvironmentFlower;
        }
        if (name.equals(E_Levels.EnviromentLoyal.toString())){
            return E_Levels.EnviromentLoyal;
        }
        if (name.equals(E_Levels.SeniorEnvironmentalist.toString())){
            return E_Levels.SeniorEnvironmentalist;
        }
        return E_Levels.waiting_for_confirmation;


    }

}