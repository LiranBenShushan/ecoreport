package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

import java.sql.Time;

public class ReportItem implements ClusterItem {
    private final E_Locations category;
    private final String type;
    private final String userMail;
    private final LatLng position;
    private final Integer intensity; // 1-very low , 5-very dominant
    private final String description;
    private final String reportTime;
    private final E_Levels confirmedBy;

    public ReportItem(E_Locations category, String type, String userMail, double lat, double lng, Integer intensity, String description, String reportTime, E_Levels confirmedBy) {
        this.category = category;
        this.type = type;
        this.userMail = userMail;
        this.position = new LatLng(lat, lng);
        this.intensity = intensity;
        this.description = description;
        this.reportTime = reportTime;
        this.confirmedBy = confirmedBy;
    }

    public E_Locations getCategory() {
        return category;
    }

    public String getType() {
        return type;
    }

    public String getUserMail() {
        return userMail;
    }

    public LatLng getPosition() {
        return position;
    }

    @Nullable
    @Override
    public String getTitle() {
        return null;
    }

    @Nullable
    @Override
    public String getSnippet() {
        return null;
    }
    public E_Levels getConfirmedBy() {
        return confirmedBy;
    }
    public Integer getIntensity() {
        return intensity;
    }

    public String getDescription() {
        return description;
    }
    public String getReportTime() {
        return reportTime;
    }


}
