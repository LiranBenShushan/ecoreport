package com.example.myapplication;

import android.content.Context;
import android.os.AsyncTask;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.google.maps.android.ui.IconGenerator;

public class MarkerClusterRenderer extends DefaultClusterRenderer<DropOnMapItem> {
    private static final int MARKER_DIMENSION = 48;
    private final IconGenerator iconGenerator;
    private final ImageView markerImageView;

    public MarkerClusterRenderer(Context context, GoogleMap map, ClusterManager<DropOnMapItem> clusterManager) {
        super(context, map, clusterManager);
        iconGenerator = new IconGenerator(context);
        markerImageView = new ImageView(context);
        markerImageView.setLayoutParams(new ViewGroup.LayoutParams(MARKER_DIMENSION, MARKER_DIMENSION));
        iconGenerator.setContentView(markerImageView);
    }
    @Override
    protected void onBeforeClusterItemRendered(DropOnMapItem item, MarkerOptions markerOptions) {
//        markerImageView.setImageResource(R.drawable.ear);
//        Bitmap icon = iconGenerator.makeIcon();
        if(item.getKindOfLocation().equals(E_Locations.Sound_Report))
            markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE));
        else if (item.getKindOfLocation().equals(E_Locations.Smell_Report))
            markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
        else if (item.getKindOfLocation().equals(E_Locations.Igud_Station))
            markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.satellite));
    }


}
