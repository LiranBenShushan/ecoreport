# "EcoReport" Android App
## Public trust in air quality data

•	Developed an Android app for public access to and interfacing with environmental data from the Haifa Conurbation. The app displays air quality data and allows users to report environmental hazards.

•	APIs: Google Maps API, Google Sign-In API

•	Worked in collaboration with the CEO, chairman and professionals from the Haifa Conurbation ("Egud Arim Haifa") for implementing the application in their organization.

•	Student collaboration between University of Haifa and MIT University.

•	The app was commended by the university’s Information Systems Department in final projects ceremony ("Seminaryon") of the department.